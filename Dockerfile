FROM python:alpine
EXPOSE 80

RUN pip install gunicorn ics requests flask
COPY src app

WORKDIR app
CMD ["gunicorn", "--workers", "4", "--bind", "0.0.0.0:80", "--access-logfile", "-", "entrypoint:app"]
