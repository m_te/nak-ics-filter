from ics import Calendar, Event
from ics.grammar.parse import Container, ContentLine
import requests
import urllib.parse
import re
from flask import *

app = Flask(__name__)


def _get_nak_cal(centuria: str) -> Calendar | None:
    if not re.match(r'[AITWBE]\d{2}[a-z]', centuria, re.IGNORECASE):
        return None
    
    for semester in range(1, 8):
        req = requests.get(f"https://cis.nordakademie.de/fileadmin/Infos/Stundenplaene/{centuria}_{semester}.ics")
        if req.status_code == 200:
            cal = Calendar(req.text)
            cal.extra = Container("VCALENDAR", ContentLine(name="NAME",         value="NORDAKADEMIE Stundenplan"),
                                               ContentLine(name="X-WR-CALNAME", value="NORDAKADEMIE Stundenplan"))
            return cal;
    return None

def _event_filter(event: Event, filters: [str]) -> bool:
    name = event.name.lower()
    i = 0
    while i < len(filters):
        key = filters[i].lower()
        key = 'v ' + key if re.match(r'\w\d{3}', key) else key
        values = filters[i+1].lower().split(',')
        if name.startswith(key) and not any(val.strip() in name for val in values):
            return False
        i += 2
    return True

@app.route('/<string:centuria>')
@app.route('/<string:centuria>/')
def cal_unfiltered(centuria: str):
    nak_cal = _get_nak_cal(centuria)
    if nak_cal is None:
        abort(404)
    
    response = make_response(nak_cal.serialize())
    response.headers.set('Content-Type', 'text/calendar')
    response.headers.set('charset', 'utf-8')
    return response


@app.route('/<string:centuria>/<path:filterpath>')
@app.route('/<string:centuria>/<path:filterpath>/')
def cal_filter(centuria: str, filterpath: str):
    filters = filterpath.split('/')
    if len(filters) % 2 != 0:
        abort(400)

    nak_cal = _get_nak_cal(centuria)
    if nak_cal is None:
        abort(404)

    cal = Calendar()
    cal.events = [e for e in nak_cal.events if _event_filter(e, filters)]

    response = make_response(cal.serialize())
    response.headers.set('Content-Type', 'text/calendar')
    response.headers.set('charset', 'utf-8')
    return response

@app.route('/')
def config_view():
    return send_file('static/config.html')

@app.route('/view')
@app.route('/view/<path:path>')
def cal_preview(path = 'index.html'):
    return send_from_directory('static/cal_preview', path)