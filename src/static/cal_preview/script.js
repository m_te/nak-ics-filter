const mapping = {
  dtstart: "start",
  dtend: "end",
  summary: "title",
};

const value_type_mapping = {
  "date-time": (input) => {
    var tzoffset = (new Date()).getTimezoneOffset() * 60000;
    var input = (new Date(new Date(input) - tzoffset)).toISOString();

    if (input.substr(-3) === "T::") {
      return input.substr(0, input.length - 3);
    }
    return input;
  },
};

function load_ics(ics_data) {
  const parsed = ICAL.parse(ics_data);
  const events = parsed[2].map(([type, event_fields]) => {
    if (type !== "vevent") return;
    return event_fields.reduce((event, field) => {
      const [original_key, _, type, original_value] = field;
      const key =
        original_key in mapping ? mapping[original_key] : original_key;
      const value =
        type in value_type_mapping
          ? value_type_mapping[type](original_value)
          : original_value;
      event[key] = value;
      return event;
    }, {});
  });
  $("#calendar").fullCalendar("removeEventSources");
  $("#calendar").fullCalendar("addEventSource", events);
}

function fetch_ics_feed(url) {
  fetch(url).then((res) => {
    if (res.ok) {
      res.text().then((data) => {
        load_ics(data);
        $('#loadingIndicator').text("");
      });
    } else {
      $('#loadingIndicator').text("Ein Fehler ist aufgetreten");
    }
  });
}

$(document).ready(function () {
  $("#calendar").fullCalendar({
    header: {
      left: "prev,next today",
      center: "title",
      right: "month,agendaWeek,agendaDay,listMonth",
    },
    navLinks: true,
    editable: false,
    minTime: "7:00:00",
    maxTime: "21:00:00",
    timezone: "Europe/Berlin",
    locale: "de",
    defaultView: "agendaWeek",
  });
  const url_feed = URIHash.get("feed");
  if (url_feed) {
    url=url_feed;
    console.log(`Load ${url}`);
    fetch_ics_feed(url);
    $("#eventsource").val(url);
  }
  $(window).on('hashchange', function() {
    location.reload();
  })
});
