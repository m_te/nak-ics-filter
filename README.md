# NORDAKADEMIE Calendar Filter

This project cleans up the schedule in your Calendar App by filtering lessons you do not need.

At the Nordakademie students of one centuria attend most of their classes together. But to every rule there is an exception.
Thus there are usually two different english classes per centuria and a load of different electives.
This can make the default subscribable calendar really messy, really quickly.

This project serves as a proxy server, always pulling the latest file from Nordakademie Servers serving you a cleaned up version of it.

# Usage

In the following examples we will assume, the software is running on the base url `example.org`.

To just get your calendar without any filtering, go into your calendar app and subscribe to the calendar `webcal://example.org/<centuria>` (e.g. `webcal://example.org/A20b`).

Setting up filters is easy: You just need two things:

- The Module Identifier (e.g. V I117). This is the first letters and numbers on the calender entry.
- A filter text that is unique to the classes you want to attend. This could be (part of) your teachers name (e.g. McCabe) or the name of your elective class.

Now just open the the website, where you have hosted your copy of this project and enter these values into the input fields. In our example that would simply be [http://example.org](http://example.org).

# Deployment

To host this simple proxy server, you have two options:

## Using an existing Python WSGI / CGI solution

If you already have a webserver running, configure it to point to the file `entrypoint.py`.

## Using the provided Dockerfile

You can simply build and run this project using [Docker](https://docker.io).

```bash
docker build -t nak-ics-filter .
docker run -d --restart unless-stopped -p <desired_port>:80 --name nak-ics-filter nak-ics-filter
```

You might want to run this docker container behind an SSL-enabled Reverse Proxy like nginx. To do so, add this to your nginx configuration:
```
  server {
    [...]    # Your other nginx confugurations.
    
    # Nordakademie Calendar Filter
    location /nak-ics/ {
        proxy_pass http://127.0.0.1:<desired_port>/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host $the_host/nak-ics;
        proxy_set_header X-Forwarded-Proto $the_scheme;

        access_log  /var/log/nginx/nak-ics-filter.access.log;
        error_log  /var/log/nginx/nak-ics-filter.error.log;
    }
  }
```